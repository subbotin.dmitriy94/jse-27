package com.tsconsulting.dsubbotin.tm.command.data;

import com.tsconsulting.dsubbotin.tm.dto.Domain;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

public class DataBinLoadCommand extends AbstractDataCommand {

    @Override
    public @NotNull String name() {
        return "data-load-bin";
    }

    @Override
    public @NotNull String description() {
        return "Load binary data.";
    }

    @Override
    public void execute() throws AbstractException, IOException, ClassNotFoundException {
        @NotNull final InputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

}
