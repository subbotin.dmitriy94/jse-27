package com.tsconsulting.dsubbotin.tm.exception.empty;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Empty index entered.");
    }

}
