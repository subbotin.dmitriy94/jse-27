package com.tsconsulting.dsubbotin.tm.command;

import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@NotNull final Task task) {
        TerminalUtil.printMessage("Id: " + task.getId() + "\n" +
                "Name: " + task.getName() + "\n" +
                "Project id: " + task.getProjectId() + "\n" +
                "Description: " + task.getDescription() + "\n" +
                "Status: " + task.getStatus().getDisplayName() + "\n" +
                "Create date: " + task.getCreateDate() + "\n" +
                "Start date: " + task.getStartDate()
        );
    }

}
