package com.tsconsulting.dsubbotin.tm.command.data;

import com.tsconsulting.dsubbotin.tm.dto.Domain;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Files;
import java.util.Base64;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @Override
    public @NotNull String name() {
        return "data-save-base64";
    }

    @Override
    public @NotNull String description() {
        return "Save base64 data.";
    }

    @Override
    public void execute() throws AbstractException, IOException {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();
        @NotNull final byte[] bytesData = byteArrayOutputStream.toByteArray();
        @NotNull final String base64Data = new String(Base64.getEncoder().encode(bytesData));
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_BASE64);
        fileOutputStream.write(base64Data.getBytes());
        fileOutputStream.close();
    }

}
