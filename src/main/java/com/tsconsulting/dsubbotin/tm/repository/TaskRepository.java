package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.Task;
import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public boolean existById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        findById(userId, id);
        return true;
    }

    @Override
    @NotNull
    public Task findByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        return findAll(userId).stream()
                .filter(t -> t.getName().equals(name))
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public void removeByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        @NotNull final Optional<Task> optional = Optional.of(findByName(userId, name));
        optional.ifPresent(this::remove);
    }

    @Override
    public void updateBuyId(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        @NotNull final Optional<Task> optional = Optional.of(findById(userId, id));
        optional.ifPresent(t -> {
            t.setName(name);
            t.setDescription(description);
        });
    }

    @Override
    public void updateBuyIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        @NotNull final Task task = findByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
    }

    @Override
    public void startById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        @NotNull final Optional<Task> optional = Optional.of(findById(userId, id));
        optional.ifPresent(t -> {
            t.setStatus(Status.IN_PROGRESS);
            t.setStartDate(new Date());
        });
    }

    @Override
    public void startByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        @NotNull final Task task = findByIndex(userId, index);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
    }

    @Override
    public void startByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        @NotNull final Optional<Task> optional = Optional.of(findByName(userId, name));
        optional.ifPresent(t -> {
            t.setStatus(Status.IN_PROGRESS);
            t.setStartDate(new Date());
        });
    }

    @Override
    public void finishById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        @NotNull final Optional<Task> optional = Optional.of(findById(userId, id));
        optional.ifPresent(t -> t.setStatus(Status.COMPLETED));
    }

    @Override
    public void finishByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        @NotNull final Task task = findByIndex(userId, index);
        task.setStatus(Status.COMPLETED);
    }

    @Override
    public void finishByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        @NotNull final Task task = findByName(userId, name);
        task.setStatus(Status.COMPLETED);
    }

    @Override
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException {
        @NotNull final Optional<Task> optional = Optional.of(findById(userId, id));
        optional.ifPresent(t -> {
                    t.setStatus(status);
                    if (status == Status.IN_PROGRESS) t.setStartDate(new Date());
                });
    }

    @Override
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) throws AbstractException {
        @NotNull final Task task = findByIndex(userId, index);
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
    }

    @Override
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws AbstractException {
        @NotNull final Optional<Task> optional = Optional.of(findByName(userId, name));
        optional.ifPresent(t -> {
                    t.setStatus(status);
                    if (status == Status.IN_PROGRESS) t.setStartDate(new Date());
                });
    }

    @Override
    public void bindTaskToProjectById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        @NotNull final Optional<Task> optional = Optional.of(findById(userId, taskId));
        optional.ifPresent(t -> t.setProjectId(projectId));
    }

    @Override
    public void unbindTaskById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        @NotNull final Optional<Task> optional = Optional.of(findById(userId, id));
        optional.ifPresent(t -> t.setProjectId(null));
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return findAll(userId).stream()
                .filter(t -> id.equals(t.getProjectId()))
                .collect(Collectors.toList());

    }

    @Override
    public void removeAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        findAll(userId).stream()
                .filter(t -> id.equals(t.getProjectId()))
                .forEach(t -> t.setProjectId(null));
    }

}
