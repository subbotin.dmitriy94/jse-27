package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.AbstractOwnerEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> {

    void add(@NotNull E entity) throws AbstractException;

    void addAll(@NotNull List<E> entities);

    void remove(@NotNull String userId, @NotNull E entity) throws AbstractException;

    void clear();

    void clear(@NotNull String userId);

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @NotNull
    E findById(@NotNull String userId, @NotNull String id) throws AbstractException;

    @NotNull
    E findByIndex(@NotNull String userId, int index) throws AbstractException;

    void removeById(@NotNull String userId, @NotNull String id) throws AbstractException;

    void removeByIndex(@NotNull String userId, int index) throws AbstractException;

}