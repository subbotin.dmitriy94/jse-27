package com.tsconsulting.dsubbotin.tm.command;

import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

@Setter
public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    @Nullable
    public Role[] roles() {
        return null;
    }

    @NotNull
    public abstract String name();

    @NotNull
    public abstract String description();

    public abstract void execute() throws AbstractException, IOException, ClassNotFoundException;

    @Override
    public String toString() {
        StringBuilder sbCommandToString = new StringBuilder();
        @Nullable final String name = name();
        @Nullable final String description = description();
        if (!EmptyUtil.isEmpty(name)) sbCommandToString.append(name);
        if (!EmptyUtil.isEmpty(description)) sbCommandToString.append(" - ").append(description);
        return sbCommandToString.toString();
    }

}
